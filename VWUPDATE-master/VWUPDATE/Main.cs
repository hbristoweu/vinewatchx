﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace VWUPDATE
{
    public partial class frmUpdate : Form
    {
        string DownloadFolder = "http://perso.maskatel.net/lib/VINEWATCHX/UPDATE/";
        string[] args;

        public frmUpdate(string[] _args)
        {
            InitializeComponent();
            args = _args;
        }

        private void frmUpdate_Load(object sender, EventArgs e)
        {
            Process.Start("TASKKILL.exe","/F /IM VinewatchX.exe");
            Process.Start("TASKKILL.exe", "/F /IM VinesaucePlayer.exe");

            System.Threading.Thread.Sleep(1000); 

            foreach (string arg in args)
                switch (arg.ToUpper())
                {
                    case "-REINSTALL":
                        DownloadFolder = "http://perso.maskatel.net/lib/VINEWATCHX/INSTALL/";
                        break;
                }

            try
            {
                string version;
                using (WebClient htmlGet = new WebClient())
                {
                    version = htmlGet.DownloadString(DownloadFolder + "version.txt");
                    string updatefile = htmlGet.DownloadString(DownloadFolder + "files.txt");
                    string[] lines = updatefile.Split('\n');

                    foreach (string line in lines)
                    {
                        string filename = line.Trim();

                        if (filename.Contains('/'))
                            if (!Directory.Exists(filename.Substring(0, filename.IndexOf('/'))))
                                Directory.CreateDirectory(filename.Substring(0, filename.IndexOf('/')));

                        htmlGet.DownloadFile(DownloadFolder + filename, filename);
                    }
                }

                lbStatus.Text = "Download Successful";
            }
            catch
            {
                lbStatus.Text = "Update Failed...";
            }
        }


        public static class OmniPlayer
        {
            [DllImport("winmm.dll")]
            private static extern long mciSendString(string lpstrCommand, StringBuilder lpstrReturnString, int uReturnLength, int hwndCallback);

            private static void _open(string file)
            {
                string command = "open \"" + file + "\" type MPEGVideo alias MyMp3";
                mciSendString(command, null, 0, 0);
            }

            private static void _play()
            {
                string command = "play MyMp3";
                mciSendString(command, null, 0, 0);
            }

            private static void _stop()
            {
                string command = "stop MyMp3";
                mciSendString(command, null, 0, 0);

                command = "close MyMp3";
                mciSendString(command, null, 0, 0);
            }

            public static void Play(string Filename)
            {

                try { _stop(); }
                catch
                {
                    new object();
                }

                try { _open(Filename); }
                catch
                {
                    new object();
                }

                try { _play(); }
                catch
                {
                    new object();
                }
            }

            public static void Stop()
            {
                try { _stop(); }
                catch
                {
                    new object();
                }

            }
        }

        private void bonziPlanel_MouseClick(object sender, MouseEventArgs e)
        {
            if (File.Exists(Directory.GetCurrentDirectory() + "/Samples/hello.mp3"))
            {
                OmniPlayer.Play(Directory.GetCurrentDirectory() + "/Samples/hello.mp3");
                lbStatus.Text = "Hello there !";
            }
        }

        private void frmUpdate_FormClosed(object sender, FormClosedEventArgs e)
        {
            if(File.Exists("VinewatchX.exe"))
                Process.Start("VinewatchX.exe");
        }

    }
}
