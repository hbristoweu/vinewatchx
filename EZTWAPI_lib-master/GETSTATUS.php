<?php

// Vinesauce as the default channel
$channel = 'vinesauce';
$showname = 'true';
$channelname = '';
$service = 'twitch';

if (isset($_GET['channel'])) $channel = htmlspecialchars($_GET['channel']);
if (isset($_GET['showname'])) $showname = htmlspecialchars($_GET['showname']);
if (isset($_GET['service'])) $service = htmlspecialchars($_GET['service']);

if(strtolower($showname) == 'true')
{
	$channelname = '[' . ucfirst($channel). ']';
}


// TWITCH API
if(strtolower($service) == 'twitch'){
    $twitch_api = 'https://api.twitch.tv/kraken/streams?channel=' . strtolower($channel);
    $json_array = json_decode(@file_get_contents($twitch_api), true);

    if ($json_array['streams'] != NULL) {
        $live = true;
        foreach($json_array['streams'] as $stream) {
            $ticker = $stream['channel']['status'];
            }
    } else {
        $live = false;
    }
}        

// HITBOX API
if(strtolower($service) == 'hitbox'){
    $hitbox_api = 'http://api.hitbox.tv/media/live/' . strtolower($channel);
    $json_array = json_decode(@file_get_contents($hitbox_api), true);

    foreach($json_array['livestream'] as $stream) {

        if ($stream['media_is_live'] == 1) 
        {
        $live = true;
        }
        else
        {
        $live = false;
        }
    
        $ticker = $stream['media_status'];   
    }
}


if ($live) 
{
  $status = $channelname . ' ' . $ticker;
}
else 
{
  $status = $channelname . ' is Offline';
}

echo utf8_decode($status);
?>