<?php 

// Vinesauce as the default channel
$channel = 'vinesauce';

if (isset($_GET['channel'])) $channel = htmlspecialchars($_GET['channel']);
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>EZTWAPI</title>
</head>
<body style="padding:0 0 0 0; Margin: 0 0 0 0;">
<object width="100%" height="100%" bgcolor="#000000" data="http://www.twitch.tv/widgets/live_embed_player.swf?channel=<?php echo strtolower($channel) ?>" id="live_embed_player_flash" type="application/x-shockwave-flash">
<param value="true" name="allowFullScreen">
<param value="always" name="allowScriptAccess">
<param value="all" name="allowNetworking">
<param value="opaque" name="wmode">
<param value="http://www.twitch.tv/widgets/live_embed_player.swf" name="movie">
<param value="hostname=www.twitch.tv&amp;channel=<?php echo strtolower($channel) ?>&amp;auto_play=true&amp;start_volume=100" name="flashvars">
</object>

</body></html>