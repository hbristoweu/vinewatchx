<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>EZTWAPI - Simple PHP wrapper for Twitch/Hitbox API</title>
</head>
EZTWAPI - Simple PHP wrapper for Twitch/Hitbox APIs<br/>
<br/>
String queries:<br/>
- Get Status (one line string): http://perso.maskatel.net/lib/EZTWAPI/GETSTATUS.php?channel=channelname<br/>
- Get Twitch Game (one line string): http://perso.maskatel.net/lib/EZTWAPI/GETGAME.php?channel=channelname<br/>
Available params:<br/>
service= Twitch or Hitbox<br/>
channel= Twitch/Hitbox channel name<br/>
showname= enable/disable [channelname] before the status<br/>
<br/>
C# ex: string text = new System.Net.WebClient().DownloadString(url);<br/>
<br/>
<br/>
Page queries:<br/>
- Get Twitch FullScreen Chat: http://perso.maskatel.net/lib/EZTWAPI/GETCHAT.php?channel=channelname<br/>
- Get Twitch FullScreen Live Feed: http://perso.maskatel.net/lib/EZTWAPI/GETLIVE.php?channel=channelname<br/>
- Get Twitch FullScreen Past Broadcast: http://perso.maskatel.net/lib/EZTWAPI/GETPB.php?channel=channelname&id=12345<br/>
- Get Twitch FullScreen Highlight: http://perso.maskatel.net/lib/EZTWAPI/GETHI.php?channel=channelname&id=12345<br/>
Available params:<br/>
channel= Twitch/Hitbox channel name<br/>
id= VOD content id (on some pages)<br/>
<br/>
</body></html>