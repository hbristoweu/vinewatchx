<?php

// Vinesauce as the default channel
$channel = 'vinesauce';
$service = 'twitch';
 
if (isset($_GET['channel'])) $channel = htmlspecialchars($_GET['channel']);
if (isset($_GET['service'])) $service = htmlspecialchars($_GET['service']);


// TWITCH API
if(strtolower($service) == 'twitch')
{
    $twitch_api = 'https://api.twitch.tv/kraken/streams?channel=' . strtolower($channel);
    $json_array = json_decode(@file_get_contents($twitch_api), true);

    if ($json_array['streams'] != NULL) {
        $game = '';
        foreach($json_array['streams'] as $stream) {
            $game = $stream['game'];
            }
    } else {
        $game = '';
    }        
}

// HITBOX API
if(strtolower($service) == 'hitbox'){
    $hitbox_api = 'http://api.hitbox.tv/media/live/' . strtolower($channel);
    $json_array = json_decode(@file_get_contents($hitbox_api), true);
    $game = '';

    foreach($json_array['livestream'] as $stream) {
        if ($stream['media_is_live'] == 1) 
        {
            $game = $stream['category_name'];   
        }
    }
}


echo utf8_decode($game);
?>

