﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace VinewatchX
{
    public static class LOCALTWAPI_MK1
    {
        public static string getStatus(string _ChannelName, string _StreamingService, bool _ShowName)
        {
            WebClient webClient = new WebClient();
            string jsonQryUrl = "";
            string result = null;
            bool live = false;
            string ticker = "";
            string ProcessedChannelName = _ChannelName;

            if(_ShowName)
            {
                TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                ProcessedChannelName = "[" + textInfo.ToTitleCase(_ChannelName) + "]";
            }

            try
            {
                if (_StreamingService.ToLower() == "twitch")
                {
                    jsonQryUrl = "https://api.twitch.tv/kraken/streams?channel=" + _ChannelName.ToLower();
                    result = webClient.DownloadString(jsonQryUrl);
                    dynamic dynArray = JsonConvert.DeserializeObject<dynamic>(result);

                    if (dynArray.streams != null) {

                        if(dynArray.streams.Count > 0)
                            live = true;

                        foreach (var stream in dynArray.streams){
                            ticker = stream.channel.status;
                        }

                    } else {

                        live = false;

                    }


                }
                else if (_StreamingService.ToLower() == "hitbox")
                {
                    jsonQryUrl = "http://api.hitbox.tv/media/live/" + _ChannelName.ToLower();
                    result = webClient.DownloadString(jsonQryUrl);
                    dynamic dynArray = JsonConvert.DeserializeObject<dynamic>(result);

                    foreach (var stream in dynArray.livestream)
                    {

                        if (stream.media_is_live == 1) 
                            live = true;
                        else
                            live = false;

    
                        ticker = stream["media_status"];   
                    }

                }
                else
                    return "";

                if (live) 
                {
                    return ProcessedChannelName + " " + ticker;
                }
                else 
                {
                    return ProcessedChannelName + " is Offline";
                }

            }
            catch(Exception ex)
            { 
            return "";
            }
        }

        public static string getGame(string ChannelName, string StreamingService, bool _ShowName)
        {
            return "";
        }
    }
}
