﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Json;
using System.Text;
using VinewatchH.Forms;

namespace VinewatchH
{
    public class VinewatchHLogic
    {
        private string      hitbox_url      = @"http://api.hitbox.tv/media/live/hippie";
        private string      lastReport      = "init";
        private string      lastLastReport;
        private int         pollRate        = 30;
        private MainForm    parentForm;

        public VinewatchHLogic(MainForm t)
        {
            parentForm = t;
        }

        public void init()
        {
            bool liveState;                 //Live status of stream
            bool livePrevAlert = false;     //Alert Suppression

            while (true)
            {
                String json = getJson();

                if (json != null)
                {
                    if (extractJsonElement(json, "media_is_live") == "1")
                    {
                        liveState = true;
                        pushReports(extractJsonElement(json, "media_status"));

                        if (livePrevAlert == false || lastLastReport != lastReport)
                        {
                            livePrevAlert = true;
                            parentForm.notify(getLastReport());
                            updateFormProperties(getLastReport());
                            updateFormIcon(true);
                        }
                    }
                    else
                    {
                        liveState = false;
                        livePrevAlert = false;
                        updateFormIcon(false);
                        updateFormProperties(DateTime.Now.ToString("HH:mm:ss tt") + ": Nothing Live right now.");
                    }
                }
                else
                {
                    if (!parentForm.supressionRadioButton.Checked)
                        parentForm.notify("Hitbox: No connection Retrying in 30...");

                    liveState = false;
                    livePrevAlert = false;
                    updateFormIcon(false);
                    updateFormProperties(DateTime.Now.ToString("HH:mm:ss tt") + ": Nothing Live right now.");
                }

                if (liveState == false)
                {
                    livePrevAlert = false;
                    liveState = false;
                    updateFormIcon(false);
                    updateFormProperties(DateTime.Now.ToString("HH:mm:ss tt") + ": Nothing Live right now.");
                }

                System.Threading.Thread.Sleep(pollRate * 1000);   //30 seconds
            }       
        }

        private string  extractJsonElement(string json, string element_name)
        {
            JsonTextParser parser = new JsonTextParser();
            JsonObject obj = parser.Parse(json);

            Debug.WriteLine(obj.ToString());

            string[] metas = obj.ToString().Split(new string[] { "\"" }, StringSplitOptions.None);

            for (int i = 0; i < metas.Length; i++)
            {
                if (metas[i] == element_name)
                {
                    Debug.WriteLine(element_name + " : " + metas[i + 2]);
                    return metas[i + 2];
                }

            }

            return null;
        }
        private string  getJson()
        {
            try
            {
                using (WebClient htmlGet = new WebClient())
                    return htmlGet.DownloadString(hitbox_url);
            }
            catch (Exception)
            {
                return null;
            }

        }

        public string   getStreamURL()
        {
            return this.hitbox_url;
        }
        public void     setStreamURL(string s)
        {
            this.hitbox_url = s;
        }

        public string   getLastReport()
        {
            return this.lastReport;
        }
        public string   getLastLastReport()
        {
            return this.lastLastReport;
        }
        public void     setLastReport(string s)
        {
            this.lastReport = s;
        }
        public void     setLastLastReport(string s)
        {
            this.lastLastReport = s;
        }
        public void     pushReports(string newReport)
        {
            lastLastReport = lastReport;
            lastReport = newReport;
        }

        public int      getPollRate()
        {
            return this.pollRate;
        }
        public void     setPollRate(int n)
        {
            this.pollRate = n;
        }

        private void    updateFormProperties(string stream_title)
        {
            parentForm.notifyX(stream_title);
        }

        private void    updateFormIcon(bool isLive_bool)
        {
            if (isLive_bool)
            {
                parentForm.notificationIcon.Icon = Properties.Resources.live;
            }
            else
            {
                parentForm.notificationIcon.Icon = parentForm.notificationIconIcon;
            }
        }

    }
}
